/*============================================================================
#     FileName: wb_handle.c
#       Author: Huoty
#        Email: loveqing2013@foxmail.com
#    TencentQQ: 1346632121
#     HomePage: http://loveqing2013.blog.163.com/
#      Version: 0.0.1
#   CreateDate: 2014-02-24 10:41:29
#      History: 
#  Description: word bank handle.
============================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dtdef.h"
#include "list_handle.h"
#include "file_rw.h"


uint_t wn = 0;  //统计词库中的词条个数

/* 从文件中读入信息然后写入线性表 */
int txtf_rw(char *fn)
{
    FILE *fp;
    char buf[1024], buf_bp[1024];
    int i;  //用于判断读取的是单词还是解释
    int f;  //数组下标控制

    if ((fp = fopen(fn, "r"))  == NULL)
    {
        perror("Error open file");
        return -1;
    }

    /* 统计文件长度 */
    while (fgets(buf, sizeof(buf), fp) != NULL)
    {
        wn++;
    }
    wn /= 2;
    rewind(fp);  //将文件指针移到开头
    make_list(wn);  //创建线性表

    for (i = 0, f = 0; fgets(buf, sizeof(buf), fp) != NULL; i++)
    {
        if (i%2 == 0)
        {
            word_parse(buf, f);
            f++;  //数组下标控制
        }
        if (i%2 == 1)
        {  
            strcpy(buf_bp, buf);  //备份，以求解释个数
            trans_num(buf_bp, f-1);  //*****f-1：存下标之后f加了1
            trans_parse(buf, f-1);
        }
    }

    fclose(fp);

    return 0;
}

int binf_rw(char *fn)
{
    FILE *fp;
    int  i, j, n, m;
    char buf[1024];

    if ((fp = fopen(fn, "r")) == NULL)
    {
        perror("Error open file");
        return -1;
    }

    /* 首先读取词条个数 */
    fread(&wn, sizeof(uint_t), 1, fp);  //读取词条个数
    make_list(wn);  //创建线性表
    //printf("词条个数：%d\n", wn);

    for (i = 0; i < wn; i++)
    {
        /* 读取单词 */
        fread(&n, sizeof(int), 1, fp); //读取单词长度
        fread(buf, n, 1, fp);  //读取单词
        create_key(i, n, buf);
        //printf("单词长度：%d\n", n);
        //printf("单词内容：%s\n", buf);

        /* 读取单词的解释个数 */
        fread(&n, sizeof(int), 1, fp);  //读取解释的个数
        create_n_trans(i, n);
        //printf("解释个数：%d\n", n);

        /* 读取解释 */
        for (j = 0; j < n; j++)
        {
            fread(&m, sizeof(int), 1, fp);  //读取解释的长度
            fread(buf, m, 1, fp);
            create_trans(i, j, m, buf);
            //printf("解释 %d 长度：%d\n", j, m);
        }
    }

    fclose(fp);

    return 0;
}

int readme_rw(void)
{
    FILE *fp;
    char buf[1024];

    if ((fp = fopen("readme", "r")) == NULL)
    {
        perror("Error open readme");
        return -1;
    }

    while (fgets(buf, sizeof(buf), fp)  != NULL)
    {
        printf("      ");
        printf("%s", buf);
    }

    return 0;
}


#ifdef DEBUG
/*************************************************************************
 * FuncName: main
 *    Input: None
 *   Output: None
 *  Val-Res: None
 *   Return: 0
 *    Brief: Main program body!
*************************************************************************/
int main(void)
{
	

    return 0;
}
#endif

/************************ (C) COPYRIGHT HOUTY PRIVATE ********END OF FILE****/

