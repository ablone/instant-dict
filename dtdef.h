/*============================================================================
#     FileName: dtdef.h
#       Author: Huoty
#        Email: loveqing2013@foxmail.com
#    TencentQQ: 1346632121
#     HomePage: http://loveqing2013.blog.163.com/
#      Version: 0.0.1
#   CreateDate: 2014-02-24 10:31:37
#      History: 
#  Description: The data type define.
============================================================================*/

#ifndef DTDEF_H
#define DTDEF_H

typedef unsigned int uint_t;

typedef struct{
    char *key;
    int n_trans;
    char **trans;
}word_t;

#endif


/*********************** (C) COPYRIGHT HOUTY PRIVATE *****END OF FILE****/

