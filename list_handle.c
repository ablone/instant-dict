/*============================================================================
#     FileName: list_handle.c
#       Author: Huoty
#        Email: loveqing2013@foxmail.com
#    TencentQQ: 1346632121
#     HomePage: http://loveqing2013.blog.163.com/
#      Version: 0.0.1
#   CreateDate: 2014-02-24 23:00:11
#      History: 
#  Description: 线性表处理
============================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dtdef.h"
#include "file_rw.h"
#include "list_handle.h"

/*extern uint_t wn;*/

static word_t *dict;  //定义一个线性表

void make_list(int n)
{
    dict = malloc(n * sizeof(word_t));
}

static void free_list(void)
{
    free(dict);
    dict = NULL;
}

static void make_key(int i, int n)
{
    dict[i].key = malloc((n) * sizeof(char));  //注意‘\0’结束符
}

static void free_key(int i)
{
    free(dict[i].key);
    dict[i].key = NULL;
}

static void make_trans(int i, int n)
{
    dict[i].trans = malloc(n * sizeof(char *));
}

static void free_trans(int i)
{
    free(dict[i].trans);
    dict[i].trans = NULL;
}

static void make_str(int i, int j, int n)
{
    dict[i].trans[j] = malloc((n) * sizeof(char));
}

static void free_str(int i, int j)
{
    free(dict[i].trans[j]);
    dict[i].trans[j] = NULL;
}

/* 统计解释的个数 */
void trans_num(char *s, int n)
{
    int i = 0;

    if (strtok(s, "@") == NULL)  //如果s为空
    {
        puts("strtok failed: list_handle.c -> trans_num  __HUOTY");
        exit(-1);
    }
    else
        i++;

    while (strtok(NULL, "@") != NULL)
        i++;

    dict[n].n_trans = i;
}

void word_parse(char *s, int n)
{
    make_key(n, strlen(s+1) + 1);
    strcpy(dict[n].key, s+1);
}

void trans_parse(char *s, int n)
{
    char *p = NULL;
    int i = 0;

    make_trans(n, dict[n].n_trans);

    p = strtok(s, "@");
    make_str(n, i, strlen(p) + 1);
    strcpy(dict[n].trans[i++], p);
//  printf("%s", dict[n].trans[i-1]);

    while ((p = strtok(NULL, "@")) != NULL)
    {
        make_str(n, i, strlen(p) + 1);
        strcpy(dict[n].trans[i++], p);
 //     printf("%s", dict[n].trans[i-1]);
    }
}

/*************** 二进制文件操作功能函数 ***************/
void create_key(int i, int n, char *s)
{
    make_key(i, n);
    strcpy(dict[i].key, s);
}

void create_n_trans(int i, int n)
{
    dict[i].n_trans = n;

    /* 为trans开辟空间，以存放各解释的首地址 */
    make_trans(i, n);  
}

void create_trans(int i, int j, int m, char *buf)
{
    make_str(i, j, m);
    strcpy(dict[i].trans[j], buf);
}
/*--------------------------------------- end --------*/

/* 释放掉所有动态申请的内存 */
void destroy(void)
{
    int i, j;

    for (i = 0; i < wn; i++)
    {
        free_key(i);
        for (j = 0; j < dict[i].n_trans; j++)
            free_str(i, j);
        free_trans(i);
    }

    free_list();

    /* 将wn置为0，彻底销毁线性表 */
    wn = 0;
}

/* 归并 */
static void merge(int l, int m, int r)
{
	int i, j, k;
	word_t b[r+1];

	for (i = l; i <= r; i++)
		b[i] = dict[i];	// init b[] = a[]
	
    i = l;	
    j = m + 1;	
    k = l;
	
    while (i <= m && j <= r)
    {
		if (strcmp(b[i].key, b[j].key) < 0)	
            dict[k++] = b[i++];
		else				
            dict[k++] = b[j++];
    }

	while (i <= m)			
        dict[k++] = b[i++];
	while (j <= r)			
        dict[k++] = b[j++];
}

/* 归并排序 */
void sort(int l, int r)
{
	int mid;

	if (l >= r)
		return;

	mid = (l+r) / 2;
	
    sort(l, mid);
	sort(mid+1, r);
	merge(l, mid, r);
}

/* 循环实现二分查找 */
static int search(int l, int r, const char *key)
{
    int mid;

    while (l <= r)
    {
        mid = (l+r) / 2;
        if (strcmp(key, dict[mid].key) < 0)
            r = mid - 1;
        else if (strcmp(key, dict[mid].key) > 0)
            l = mid + 1;
        else
            return mid;  //找到返回所在坐标
    }

    return -1;  //没找到返回-1
}

void print_trans(const char *s)
{
    int n, i;
    char key_bp[512];  

    system("clear");
    puts("\n======================== 单词查询 ========================\n");

    if ((n = search(0, wn-1, s)) == -1)
    {
        /* 去除单词末尾多余的‘\n’ */
        strcpy(key_bp, s);
        *(key_bp + strlen(key_bp) - 1) = '\0';

        printf("\n\t*** No search \"%s\" ! ***\n", key_bp);
        return;
    }

    /* 去除单词末尾多余的‘\n’ */
    strcpy(key_bp, dict[n].key);
    *(key_bp + strlen(key_bp) - 1) = '\0';
    
    printf("\n  +---##  %s  ##----*********************************@\n", key_bp);
    printf("  |\n");
    
    printf("  | %s", dict[n].trans[0]);
    if (dict[n].n_trans == 1)
        ;
    else
        putchar(10);
    
    for (i = 1; i < dict[n].n_trans; i++)
    {
        printf("  |       %s", dict[n].trans[i]);
        if ( i < dict[n].n_trans - 1)
            putchar(10);
    }
    
    puts("  |");
    puts("  |                                         All rights reserved @@");
    puts("  ***************************************************************@\n");
}

int create_binf(char *fn)
{
    FILE *fp;
    int i, j, l;

    if ((fp = fopen(fn, "w")) == NULL)
    {
        perror("Error open file, in create_binf !");
        return -1;
    }

    /* 首先写入词条个数 */
    fwrite(&wn, sizeof(uint_t), 1, fp);

    for (i = 0; i < wn; i++)
    {
        /* 写入单词 */
        l = strlen(dict[i].key) + 1; //*求单词的长度,包括‘\0’
        fwrite(&l, sizeof(int), 1, fp);   //写入单词长度
        fwrite(dict[i].key, l, 1, fp);  //写入单词

        /* 写入单词的解释 */
        fwrite(&(dict[i].n_trans), sizeof(int), 1, fp);  //写入解释个数
        for (j = 0; j < dict[i].n_trans; j++)
        {
            l = strlen(dict[i].trans[j]) + 1;  //*求解释的长度,加上‘\0’
            fwrite(&l, sizeof(int), 1, fp);  //写入解释的长度
            fwrite(dict[i].trans[j], l, 1, fp);  //写入解释的内容
        }

    }

    fclose(fp);

    return 0;
}




#ifdef DEBUG
/*************************************************************************
 * FuncName: main
 *    Input: None
 *   Output: None
 *  Val-Res: None
 *   Return: 0
 *    Brief: Main program body!
*************************************************************************/
int main(void)
{
	

    return 0;
}
#endif

/************************ (C) COPYRIGHT HOUTY PRIVATE ********END OF FILE****/

