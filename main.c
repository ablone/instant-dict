#include <stdio.h>
#include "dtdef.h"
#include "file_rw.h"
#include "list_handle.h"
#include "myshell.h"

extern uint_t wn;

/*************************************************************************
 * FuncName: main
 *    Input: None
 *   Output: None
 *  Val-Res: None
 *   Return: 0
 *    Brief: Main program body!
*************************************************************************/
int main(void)
{
    /* 让程序启动时默认载入二进制索引词库dict.bin */
    if (binf_rw("dict.bin") == -1)
    {
        printf("载入默认词库失败！");
        sleep(2);
    }

    show_gui();

    destroy();

    return 0;
}


/************************ (C) COPYRIGHT HOUTY PRIVATE ********END OF FILE****/

