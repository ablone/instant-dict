/*============================================================================
#     FileName: list_handle.h
#       Author: Huoty
#        Email: loveqing2013@foxmail.com
#    TencentQQ: 1346632121
#     HomePage: http://loveqing2013.blog.163.com/
#      Version: 0.0.1
#   CreateDate: 2014-02-24 23:01:00
#      History: 
#  Description: 线性标处理头文件
============================================================================*/

#ifndef LIST_HANDLE_H
#define LIST_HANDLE_H

extern void make_list(int n);
extern void trans_num(char *, int);
extern void word_parse(char *, int);
extern void trans_parse(char *, int);
extern void destroy(void);
extern void sort(int, int);
extern void print_trans(const char *);

/* 用于二进制文件读取 */
extern void create_key(int, int, char *);
extern void create_n_trans(int, int);
extern void create_trans(int, int, int, char *);

extern int create_binf(char *);

#endif


/*********************** (C) COPYRIGHT HOUTY PRIVATE *****END OF FILE****/

